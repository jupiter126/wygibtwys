#!/usr/bin/env bash
# Copyright 2019 Nelson-Jean Gaasch
# Licence: (New BSD Licence)
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
##############
#version=0.074
# https://triptico.com/twtxt.txt
##############
pproject=$1;ccommand=$2;aarg=$3;canary=$4
def='\033[0m'; red='\033[1;31m'; gre='\033[1;32m'; yel='\033[1;33m'; blu='\033[1;34m'; mag='\033[1;35m'; cya='\033[1;36m'
if [[ "x$canary" != "x" ]]; then printf "%b You killed the canary! (Too many parameters) %b\n" "$red" "$def" && exit 1;fi
debug="1" # set on "1" for debug information
ddate=$(date +%Y%m%d) && dddate=$(date +%Y%m%d-%H%M) && twtxtdate=$(date +%Y-%m-%dT%H:%M:%SZ) && ppubdate=$(date -R)
ssystem=$(uname)
if [[ "$ssystem" = "FreeBSD" ]]; then
	cccleaner="/usr/local/bin/tidy5"
	sssed='sed -i ""'
	tttac="tail -r"
	lllinkch="linkchecker --no-status"
	function f_keepsmallest {
		if [[ "$(stat -f %z "$1")" -le "$(stat -f %z "$2")" ]]; then rm "$2" && return 0;else return 1;fi
	}
elif [[ "$ssystem" = "Linux" ]]; then
	cccleaner="/usr/local/bin/tidy"
	sssed="sed -i"
	tttac="tac"
	lllinkch="linkchecker --no-status --check-html --check-css"
	function f_keepsmallest {
		if [[ "$(stat --printf="%s" "$1")" -le "$(stat --printf="%s" "$2")" ]]; then rm "$2" && return 0;else return 1;fi
	}
fi
function f_help { # Displays "help"
	printf " %b./wygibtwys.sh projectname command argument %b\n" "$yel" "$def"
	printf " %bCommand argument can be:%b" "$gre" "$def"
	printf "%b - init projectname %b to initialise projectname%b\n" "$def" "$red" "$def"
	printf "                         %b - createpage <pagename_with_no_spaces> %b to initialise a new page%b\n" "$def" "$red" "$def"
	printf "                         %b - createpost <postname_with_no_spaces> %b to initialise a new post%b\n" "$def" "$red" "$def"
	printf "                         %b - build %b To quickly build the source%b\n" "$def" "$red" "$def"
	printf "                         %b - optim %b To build, minify, checklinks%b\n" "$def" "$red" "$def"
	printf "                         %b - prod %b  To build, minify, and replace production variables%b\n" "$def" "$red" "$def"
	exit 1
}
function f_debug { # Debug mode helps tracing where crashes occur (if $debug = 1)
       aaarg="$1"
       if [ "$debug" = "1" ]; then
               printf "%b %s %b %s %b\n" "$mag" "${FUNCNAME[*]}" "$red" "$aaarg" "$def" && aaarg=""
       fi
}
function f_createpage { # Creates default resource files for a new page, and declares it in projects/$pproject/pagelist
	f_debug "$pproject $aarg"
	if [[ ! -f "projects/$pproject/source/$aarg.body" ]]; then
		cp "projects/$pproject/source/template/meta" "projects/$pproject/source/$aarg.meta"
		# cp projects/$pproject/source/template/rss projects/$pproject/source/$aarg.rss #no RSS for pages only posts (RSS was designed for NEWS)
		cp "projects/$pproject/source/template/body" "projects/$pproject/source/$aarg.body"
		cp "projects/$pproject/source/template/sitemap" "projects/$pproject/source/$aarg.sitemap"
		cp "projects/$pproject/source/template/javascript" "projects/$pproject/source/$aarg.js"
		echo "$aarg" >> "projects/$pproject/pagelist"
	else
		echo "$aarg already seems to exist, aborting creation."
	fi
}
function f_createpost { # Creates default resource files for a new post, and declares it in projects/$pproject/postlist
	f_debug "$pproject $aarg"
	mkdir -p "projects/$pproject/source/posts"
	if [[ "$aarg" = "index" ]]; then
		echo "Not creating index, postname reserved" && return 1
	fi
	if [[ ! -f "projects/$pproject/source/posts/$aarg.body" ]]; then
		cp "projects/$pproject/source/template/rss" "projects/$pproject/source/posts/$ddate-$aarg.rss"
		eval "$sssed" "s/ppubdate/'$ppubdate'/" "projects/$pproject/source/posts/$ddate-$aarg.rss"
		cp "projects/$pproject/source/template/meta" "projects/$pproject/source/posts/$ddate-$aarg.meta"
		cp "projects/$pproject/source/template/body" "projects/$pproject/source/posts/$ddate-$aarg.body"
		cp "projects/$pproject/source/template/sitemap" "projects/$pproject/source/posts/$ddate-$aarg.sitemap"
		cp "projects/$pproject/source/template/javascript" "projects/$pproject/source/posts/$ddate-$aarg.js"
		cp "projects/$pproject/source/template/post.index" "projects/$pproject/source/posts/$ddate-$aarg.index"
		echo "Misc" > "projects/$pproject/source/posts/$ddate-$aarg.cat"
		echo "$ddate-$aarg" >> "projects/$pproject/postlist"
	else
		echo "$aarg already seems to exist, aborting creation." && return 1
	fi
}
function f_initrss {
f_debug
	sed -e "s;ppubdate;${ppubdate};" < "projects/$pproject/source/rssbase" > "projects/$pproject/$ccommand/rss"
}
function f_closerss {
f_debug
cat >> "projects/$pproject/$ccommand/rss" <<EOL
 </channel>
</rss>
EOL
}
function f_initsitemap {
f_debug
cat > "projects/$pproject/$ccommand/sitemap.xml" <<EOL
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
EOL
}
function f_closesitemap {
f_debug
	echo '</urlset>' >> "projects/$pproject/$ccommand/sitemap.xml"
	if [[ "$ccommand" = "optim" ]] ; then eval "$sssed" "s,$devdom,$optimdom,g" "projects/$pproject/$ccommand/sitemap.xml"
	elif [[ "$ccommand" = "prod" ]] ; then eval "$sssed" "s,$devdom,$proddom,g" "projects/$pproject/$ccommand/sitemap.xml"
	fi
}
function f_build {
f_debug "$pproject $ccommand"
	f_optimg
	rm -Rf "projects/$pproject/$ccommand" 2>/dev/null && mkdir -p "projects/$pproject/$ccommand/posts"
	rsync -a "projects/$pproject/source/css/" "projects/$pproject/$ccommand/css" --delete
	rsync -a "projects/$pproject/source/php/" "projects/$pproject/$ccommand/php" --delete
	rsync -a "projects/$pproject/source/js/" "projects/$pproject/$ccommand/js" --delete
	rsync -a "projects/$pproject/source/images/" "projects/$pproject/$ccommand/images" --delete
	rsync -a "projects/$pproject/source/assets/" "projects/$pproject/$ccommand/assets" --delete
	f_initrss
	f_initsitemap
	### build all pages
	while read -r ppagename; do
		touch "projects/$pproject/$ccommand/$ppagename.html"
		for i in "projects/$pproject/source/commonheader" "projects/$pproject/source/$ppagename.meta" "projects/$pproject/source/commoncss" "projects/$pproject/source/commonmenu" "projects/$pproject/source/$ppagename.body" "projects/$pproject/source/commonfooter" "projects/$pproject/source/$ppagename.js" "projects/$pproject/source/commoneof"; do
			cat "$i" >> "projects/$pproject/$ccommand/$ppagename.html"
		done
		cat "projects/$pproject/source/$ppagename.sitemap" >> "projects/$pproject/$ccommand/sitemap.xml"
	done < "projects/$pproject/pagelist"
### init categories
	bigtaglist=('<div class="center"><br>')
	for ccat in $(cat projects/"$pproject"/source/posts/*.cat|sort -u); do if [[ "$ccat" = "" ]]; then continue; fi
		bigtaglist+=("#<a href=$devdom/posts/index-$ccat.html> $ccat </a>#")
		touch "projects/$pproject/$ccommand/posts/index-$ccat.html"
		for i in "projects/$pproject/source/commonheader" "projects/$pproject/source/posts/index.meta" "projects/$pproject/source/commoncss" "projects/$pproject/source/commonmenu"; do
			cat "$i" >> "projects/$pproject/$ccommand/posts/index-$ccat.html"
		done
		echo "<div class=\"container\"><blockquote><H2><b>#$ccat</b></H2></blockquote></div><ul>" >> "projects/$pproject/$ccommand/posts/index-$ccat.html"
	done
	bigtaglist+=("</div>")
### build all posts
	for ppostname in $($tttac "projects/$pproject/postlist"); do
		taglist=('<div class="center"><br>')
		while read -r ccat; do if [[ "$ccat" = "" ]]; then continue; fi
			taglist+=("#<a href=$devdom/posts/index-$ccat.html> $ccat </a>#")
			cat "projects/$pproject/source/posts/$ppostname.index" >> "projects/$pproject/$ccommand/posts/index-$ccat.html"
		done < "projects/$pproject/source/posts/$ppostname.cat"
		taglist+=("</div>")
		touch "projects/$pproject/$ccommand/posts/$ppostname.html"
		for i in "projects/$pproject/source/commonheader" "projects/$pproject/source/posts/$ppostname.meta" "projects/$pproject/source/commoncss" "projects/$pproject/source/commonmenu"; do
			cat "$i" >> "projects/$pproject/$ccommand/posts/$ppostname.html"
		done
		echo "${taglist[*]}" >> "projects/$pproject/$ccommand/posts/$ppostname.html"
		for i in "projects/$pproject/source/posts/$ppostname.body" "projects/$pproject/source/commonfooter" "projects/$pproject/source/posts/$ppostname.js" "projects/$pproject/source/commoneof"; do
			cat "$i" >> "projects/$pproject/$ccommand/posts/$ppostname.html"
		done
		cat "projects/$pproject/source/posts/$ppostname.sitemap" >> "projects/$pproject/$ccommand/sitemap.xml"
		cat "projects/$pproject/source/posts/$ppostname.rss" >> "projects/$pproject/$ccommand/rss"
	done
### Close Categories
	for ccat in $(cat projects/"$pproject"/source/posts/*.cat|sort -u); do if [[ "$ccat" = "" ]]; then continue; fi
		echo "</ul>" >> "projects/$pproject/$ccommand/posts/index-$ccat.html"
			for i in "projects/$pproject/source/commonfooter" "projects/$pproject/source/posts/index.js" "projects/$pproject/source/commoneof"; do
				cat "$i" >> "projects/$pproject/$ccommand/posts/index-$ccat.html"
			done
	done
###build postindex
	touch "projects/$pproject/$ccommand/posts/index.html"
	for i in "projects/$pproject/source/commonheader" "projects/$pproject/source/posts/index.meta" "projects/$pproject/source/commoncss" "projects/$pproject/source/commonmenu"; do
		cat "$i" >> "projects/$pproject/$ccommand/posts/index.html"
	done
	echo "${bigtaglist[*]}" >> "projects/$pproject/$ccommand/posts/index.html"
	echo "<div class=\"container\"><blockquote><H2><b>News</b></H2></blockquote></div><ul>" >> "projects/$pproject/$ccommand/posts/index.html"
	for i in $(ls -r projects/"$pproject"/source/posts/*.index); do cat "$i" >> "projects/$pproject/$ccommand/posts/index.html"; done #keep ls -r to have index in right order
	echo "</ul>" >> "projects/$pproject/$ccommand/posts/index.html"
	for i in "projects/$pproject/source/commonfooter" "projects/$pproject/source/posts/index.js" "projects/$pproject/source/commoneof"; do
		cat "$i" >> "projects/$pproject/$ccommand/posts/index.html"
	done
### end build postindex
	f_closesitemap
	f_closerss
	f_cleaner
	for i in $(find "projects/$pproject/source/" -maxdepth 1 -type f -name "yandex*") "projects/$pproject/source/BingSiteAuth.xml" "projects/$pproject/source/robots.txt" "projects/$pproject/source/.htaccess"; do
		if [[ -f "$i" ]]; then cp "$i" "projects/$pproject/$ccommand/"
		else echo "$i is missing, I would probably add it there somewhere"
		fi
	done
	if [[ "$ccommand" = "optim" ]] ; then
		if ! f_linkchecker; then exit 1; fi
	fi
	if [[ "$ccommand" = "prod" ]]; then
		f_custom
		printf "%b Creating production archive.%b\n" "$blu" "$def" && tar -czf "projects/$pproject/archive/$pproject.prod_$dddate.tar.gz" "projects/$pproject/$ccommand"
	fi
}
function f_optim {
	fffile="$1"
	f_debug "$fffile"
	if [[ "$ccommand" = "optim" ]] ; then eval "$sssed" "s,$devdom,$optimdom,g" "$fffile"
	elif [[ "$ccommand" = "prod" ]] ; then eval "$sssed" "s,$devdom,$proddom,g" "$fffile"
	fi
	if [[ "$fffile" != "projects/$pproject/$ccommand/rss" ]] && [[ "$fffile" != "projects/$pproject/$ccommand/sitemap.xml" ]] && [[ "$fffile" != "projects/$pproject/$ccommand/css/style.css" ]]; then
		$cccleaner -w 5000 -q -m "$fffile"
	fi
	if [[ "$(grep dddefault "$fffile")" != "" ]]; then
		f_debug "$fffile contains \"dddefault\", change it for SEO purposes" && grep dddefault "$fffile"
	fi
	if [[ "$ccommand" = "optim" ]] || [[ "$ccommand" = "prod" ]]; then
		if [[ "$fffile" != "projects/$pproject/$ccommand/rss" ]]; then
			minify "$fffile" > "$fffile.mini" && sleep 1 &&  mv "$fffile.mini" "$fffile"
		fi
	fi
	if [[ "$fffile" != "projects/$pproject/$ccommand/css/style.css" ]] && [[ "$fffile" != "projects/$pproject/$ccommand/js/pricecalc.js" ]]; then
		gzip < "$fffile" > "$fffile.gz"
	fi
}
function f_optimg {
	f_debug
	if [[ "$optimiseimages" = "1" ]]; then
		printf "%b Patience, optimising images...%b\n" "$yel" "$def"
		if [[ ! -f "projects/$pproject/optimg.txt" ]]; then touch "projects/$pproject/optimg.txt"; fi
		optimlist=()&& while read -r imgnam; do optimlist+=("$imgnam"); done < "projects/$pproject/optimg.txt"
		for imgname in $(find "projects/$pproject/source/images/" -type f|cut -f5 -d'/'); do
			for i in "${optimlist[@]}"; do if [[ "$i" == "$imgname" ]]; then continue 2; fi; done
			ext=$(echo "$imgname"|rev|cut -f1 -d'.'|rev)
			filnam=$(echo "$imgname"|rev|cut -f2- -d'.'|rev)
			if [[ "$ext" = "jpeg" ]]; then mv "projects/$pproject/source/images/$filnam.jpeg" "projects/$pproject/source/images/$filnam.jpg" && ext="jpg"; fi
			if [[ "$ext" != "png" ]] && [[ "$ext" != "jpg" ]]; then continue 2; fi
			width=$(identify -format '%w' "projects/$pproject/source/images/$filnam.$ext")
			if [[ "$width" -gt "$maximgwidth" ]]; then convert -resize "$maximgwidth" "projects/$pproject/source/images/$filnam.$ext" "projects/$pproject/source/images/$filnam.$ext.tmp"; fi
			if [[ -f "projects/$pproject/source/images/$filnam.$ext.tmp" ]]; then
				if ! f_keepsmallest "projects/$pproject/source/images/$filnam.$ext" "projects/$pproject/source/images/$filnam.$ext.tmp"; then
							mv "projects/$pproject/source/images/$filnam.$ext.tmp" "projects/$pproject/source/images/$filnam.$ext"
				fi
			fi
			if   [[ "$ext" = "jpg" ]]; then convert "projects/$pproject/source/images/$filnam.jpg" "projects/$pproject/source/images/$filnam.png"
			elif [[ "$ext" = "png" ]]; then
				if [[ "$(identify -format '%[channels]' "projects/$pproject/source/images/$filnam.png")" != "srgba" ]]; then #don't convert png that has alpha channel (transparency)
					convert "projects/$pproject/source/images/$filnam.png" "projects/$pproject/source/images/$filnam.jpg"
				fi
			fi
			pngquant -o "projects/$pproject/source/images/$filnam.png.mini" --force --quality="$maximgqual" "projects/$pproject/source/images/$filnam.png"
			if [[ -f "projects/$pproject/source/images/$filnam.png.mini" ]]; then
				if ! f_keepsmallest "projects/$pproject/source/images/$filnam.png" "projects/$pproject/source/images/$filnam.png.mini";
					then mv "projects/$pproject/source/images/$filnam.png.mini" "projects/$pproject/source/images/$filnam.png";
				fi
			fi
			if [[ -f "projects/$pproject/source/images/$filnam.jpg" ]]; then
				jpegoptim -q -m"$maximgqual" "projects/$pproject/source/images/$filnam.jpg"
				if ! f_keepsmallest "projects/$pproject/source/images/$filnam.png" "projects/$pproject/source/images/$filnam.jpg"; then ext="jpg" && rm "projects/$pproject/source/images/$filnam.png";else ext="png"; fi
			fi
			echo "$filnam.$ext" >> "projects/$pproject/optimg.txt"
			if [[ "$filnam.$ext" != "$imgname" ]]; then
				for anyfile in projects/"$pproject"/source/*.body projects/"$pproject"/source/*.meta projects/"$pproject"/source/*.sitemap projects/"$pproject"/source/common* projects/"$pproject"/source/posts/*; do
					eval "$sssed" "s/$imgname/$filnam.$ext/g" "$anyfile"
				done
			fi
		done
	else
		printf "%b Not optimising images%b\n" "$yel" "$def"
	fi
}
function f_cleaner {
	f_debug
	for ffile in $(ls projects/"$pproject/$ccommand"/*.html) $(ls projects/"$pproject/$ccommand"/posts/*.html) "projects/$pproject/$ccommand/rss" "projects/$pproject/$ccommand/css/style.css"; do
		f_optim "$ffile"
	done
	rsync -a "projects/$pproject/source/images/" "projects/$pproject/$ccommand/images" --delete
### Check for unused images --> commented cause very resource consuming
#	for iiimage in $(ls projects/$pproject/$ccommand/images/); do
#		if [[ "$(grep -R $iiimage projects/$pproject/$ccommand/)" = "" ]] && [[ "$iiimage" != "browserconfig.xml" ]] && [[ "$iiimage" != "dddefault.png" ]] && [[ "$iiimage" != "apple-icon.png" ]] && [[ "$iiimage" != "apple-icon-precomposed.png" ]]; then
#			echo "$yel Warning: $def $iiimage unused (deleting from build if $ccommand=prod)"
#			if [[ "$ccommand" = "prod" ]]; then
#				rm "projects/$pproject/$ccommand/images/$iiimage"
#			fi
#		fi
#	done
	if [[ "$ccommand" = "optim" ]] || [[ "$ccommand" = "prod" ]] ; then
		printf "%b minifying scripts and styles%b\n" "$yel" "$def"
		for i in projects/"$pproject/$ccommand"/js/* projects/"$pproject/$ccommand"/css/*; do
			minify "$i" > "$i.min" && mv "$i.min" "$i"
			if [[ "$ccommand" = "optim" ]] ; then eval "$sssed" "s,$devdom,$optimdom,g" "$i"
			elif [[ "$ccommand" = "prod" ]] ; then eval "$sssed" "s,$devdom,$proddom,g" "$i"
			fi
	                gzip < "$i" > "$i.gz"
		done
	fi
}
function f_initproject {
	f_debug
	if [[ -d "template" ]]; then cp -R "template" "projects/$pproject" && return 0; fi
}
function f_linkchecker {
	f_debug
	printf "%b Patience - checking links ... :%b\n" "$cya" "$def"
	if $lllinkch "$optimdom"; then printf "%b link check passed%b\n" "$gre" "$def"; return 0
	else printf "%b link check failed%b\n" "$red" "$def" && return 1;
	fi
}

if [[ "$pproject" = "" ]]; then f_help
elif source "projects/$pproject/settings.txt" 2>/dev/null ; then
	if [[ "$ccommand" = "createpage" ]]; then
		if [[ "$aarg" = "" ]]; then printf "%b We're missing the page name to create page!%b\n" "$red" "$def";f_help
		else f_createpage
		fi
	elif [[ "$ccommand" = "createpost" ]]; then
		if [[ "$aarg" = "" ]]; then printf "%b We're missing the post name to create post!%b\n" "$red" "$def";f_help
		else f_createpost
		fi
	elif [[ "$ccommand" = "build" ]] || [[ "$ccommand" = "optim" ]] || [[ "$ccommand" = "prod" ]]; then
		if [[ "$aarg" != "" ]]; then printf "%b We have an unexpected argument!%b\n" "$red" "$def";f_help
		else f_build
		fi
	else printf "%b Command unknown for existing project %b\n" "$red" "$def"
	fi
elif [[ "$ccommand" = "init" ]]; then
	if [[ "$aarg" != "" ]]; then printf "%b We have an unexpected argument!%b\n" "$red" "$def";f_help
	else f_initproject
	fi
else printf "%bSame player shoot Again%b\n" "$red" "$def"
fi
exit 0
