### WYGIBTWYS

## What you get is better than what you see

**I started WYGIBTWYS on a late afternoon a few years ago, when I got tired of Wordpress**

**It is written in bash, and works on Linux and FreeBSD. There are certainly similar projects that are more efficient,**


I very slowly added a couple functions when I needed them - few are still missing.


[v] Easy site skeleton generation based on template

[v] Generates pages and posts, with category indexes for posts

[v] Generates version for dev serv, replaces links automatically in all pages for prod serv

[v] Tidy and minify the code

[v] Check internal link validity

[v] Generates sitemap, rss feed

[v] Optimise Images

[=] Pass Shellcheck (Kinda sorted)

[v] Keep codebase small


---

## Dependencies:

# Debian

    apt install tac tidy linkchecker pngquant jpegoptim imagemagick rsync
    [get minify](https://github.com/tdewolff/minify/tree/master/cmd/minify)

# FreeBSD

    pkg install tidy-html5 minify pngquant jpegoptim ImageMagick7 rsync
    pip install git+https://github.com/linkchecker/linkchecker.git

---

## usage

wygibtwys only accepts a limited, precise set of parameters:

#It is called as

*./wygibtwys.sh projectname command args*

     ./wygibtwys.sh projectname init                   # copies base required fs hierarchy for a new project from template to projects/projectname
     ./wygibtwys.sh projectname createpage pagename    # Create a new page in projectname named postname (no spaces!)
     ./wygibtwys.sh projectname createpost postname    # Create a new post in projectname named postname (no spaces!)
     ./wygibtwys.sh projectname build                  # Makes a build of the site from source to build folder (not uglified)
							                             Use for rapid development.
     ./wygibtwys.sh projectname optim                  # Makes a build of the site from source to optim folder (uglified)
                                                         Test if minimize does not screw up the site - test links.
     ./wygibtwys.sh projectname prod                   # Makes a build of the site from source to prod folder (sedded for production and uglified
                                                         then makes a tar.gz of prod into archive.
---

## settings.txt

The settings is divided in 3 parts: 

# domain options

These define the development domain, optimisation domain and production domain (seds content accordingly).

    devdom="http://dev.devdoma.in"
    optimdom="http://optim.devdoma.in"
    proddom="https://www.realdomain.com"

# Image optimisation

Define if you want images to be optimised or not as well as the max quality and max resolution you want to enforce (if enabled)

    optimiseimages="1"
    maximgqual="80"
    maximgwidth="1024"

# Production settings

Insert custom code for last time replacement in production files (like db access or CDN for js/css)

    function f_custom {
    echo "$gre No custom Settings $def"
    #echo "$gre Updating Credentials for prod server $def"
    #sed -i -e "s/testuser/user12485/" projects/$pproject/prod/php/connection.php
    #sed -i -e "s/testdb/dbvmprojectX/" projects/$pproject/prod/php/connection.php
    #sed -i -e "s/testpassword/MuHaH4hA/" projects/$pproject/prod/php/connection.php
    }

---

## Image Optimisation

This is a quite aggressive function when enabled, its objective is to reduce image size.

*!It is advised to have a backup of your images!*

1. It checks projects/$pproject/opimg.txt to see if image was not optimised yet

2. It resizes to maxwidth if file is bigger than that, then keep the smallest in size

3. If jpg it converts to png, if png without transparency, it converts to jpg

4. It uses pngquant to reduce adapt quality of png, keeps the smallest - does the same with jpegoptim and jpg

5. Compares size of jpg and png, keeps the smallest

6. It adds the name of the image in projects/$pproject/opimg.txt

**#- RTFM NOTE1: If you code the whole page with some images, when you run the next build command, it might change some extentions.  As such, your next optim command will find a whole lot of broken links... at this point these have to be fixed by hand in projects/$pproject/source/...**

**#- RTFM NOTE2: If you want one specific image not to be optimised, just add its name to projects/$pproject/opimg.txt before you run the next build (it will consider it has been optimised already)**

---

## Page structure

Each page/post is generated out of 7 parts, we MUST respect this strict structure!

__________________

|_________1_______|	--> commonheader	Common to all pages - Opening html

|_________2_______| --> pagename.meta	The page's unique meta and CSS

|_________3_______| --> commoncss		Common to all pages - css and navbar menu

|_________4_______| --> pagename.body	The page's unique body

|_________5_______| --> commonfooter	Common to all pages - footer

|_________6_______| --> pagename.js		The page's unique js

|_________7_______| --> commoneof		Common js and closing of body and html

---

## File structure

One page/post is made of the following files:

+ postname.body       -  Body of the page/post

+ postname.cat        -  categories of the post

+ postname.index      -  index of the post

+ postname.js         -  js of the page/post

+ postname.meta       -  meta of the page/post

+ postname.rss        -  rss data for the post

+ postname.sitemap    -  sitemap entry for the page/post


---

## Joined files:

As examplesite, I included a small part of my personnal website, so you see how it all comes together.

jquery and materialize css have been included in the template and example, feel free to update them and or replace with whatever framework you like to use!

---
